package yakiv.bondar.dev.intershiptask2.data;

import java.util.ArrayList;
import java.util.List;

import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;
import yakiv.bondar.dev.intershiptask2.model.HandlingCardStatus;

/**
 * Created by User on 26.07.2016.
 */
public class ModelDataContainer {

    private static List<HandlingCardModel> sHandlingCardModels = new ArrayList<>();

    static {
        sHandlingCardModels.add(new HandlingCardModel("Clean the road", "Karla Marksa",
                "September 1 2016", HandlingCardStatus.IN_PROGRESS, 0));
        sHandlingCardModels.add(new HandlingCardModel("Fix the road", "Lenina",
                "October 1 2016", HandlingCardStatus.IN_PROGRESS, 1));
        sHandlingCardModels.add(new HandlingCardModel("Mend the road", "Mira",
                "November 1 2016", HandlingCardStatus.IN_PROGRESS, 2));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Krut Herous",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 3));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Satanistov str",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 4));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Stalin alley",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 5));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Varg Vikerness",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 6));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Bandery str",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 7));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "111111111111 Go hard like Vladimir Putin street",
                "December 1 2016", HandlingCardStatus.IN_PROGRESS, 8));
        sHandlingCardModels.add(new HandlingCardModel("Clean the road", "Polia ave",
                "September 1 2016", HandlingCardStatus.DONE, 4));
        sHandlingCardModels.add(new HandlingCardModel("Fix the road", "Kalinina",
                "October 1 2016", HandlingCardStatus.DONE, 5));
        sHandlingCardModels.add(new HandlingCardModel("Mend the road", "Kalinova",
                "November 1 2016", HandlingCardStatus.DONE, 4));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Sviatoslav Horobriy",
                "December 1 2016", HandlingCardStatus.DONE, 3));
        sHandlingCardModels.add(new HandlingCardModel("Clean the road", "Polovitska",
                "September 1 2016", HandlingCardStatus.WAITING, 2));
        sHandlingCardModels.add(new HandlingCardModel("Fix the road", "Dmitra Yavornitskogo",
                "October 1 2016", HandlingCardStatus.WAITING, 1));
        sHandlingCardModels.add(new HandlingCardModel("Mend the road", "Vernadskogo",
                "November 1 2016", HandlingCardStatus.WAITING, 0));
        sHandlingCardModels.add(new HandlingCardModel("Fix Moon craters", "Uzviz Krutogirniy",
                "December 1 2016", HandlingCardStatus.WAITING, 1));
    }

    public static List<HandlingCardModel> getsHandlingCardModels() {
        return sHandlingCardModels;
    }
}
