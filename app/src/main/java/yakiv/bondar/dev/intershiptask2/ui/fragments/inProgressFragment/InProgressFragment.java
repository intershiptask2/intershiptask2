package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import yakiv.bondar.dev.intershiptask2.R;
import yakiv.bondar.dev.intershiptask2.adapters.MainDataAdapter;
import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;
import yakiv.bondar.dev.intershiptask2.utils.ItemOffsetDecoration;

/**
 * Created by User on 26.07.2016.
 */
public class InProgressFragment extends Fragment implements InProgressFragmentView{

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private List<HandlingCardModel> mModel;
    private InProgressFragmentPresenter mPresenter;
    private int mPosition;
    private View mRootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.in_progress_tab_fragment, container, false);
        initPosition();
        initPresenter();
        initModel();
        initRecyclerView();
        return mRootView;
    }

    private void initPosition() {
        mPosition = getArguments().getInt("position");
    }

    //TODO:make recycler_view layout
    //TODO:make card_view layout
    //TODO:make recycler_view adapter
    //TODO:make recycler_view ItemOffsetDecoration
    private void initRecyclerView() {
        ButterKnife.bind(this, mRootView);
        initLinearLayoutManager(recyclerView);
        ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration(getContext(),
                R.dimen.rv_between_padding);
        recyclerView.addItemDecoration(itemOffsetDecoration);
        setAdapter(recyclerView);
    }

    private void setAdapter(RecyclerView recyclerView) {
        MainDataAdapter adapter = new MainDataAdapter(mModel);
        recyclerView.setAdapter(adapter);
    }

    private void initLinearLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initPresenter() {
        mPresenter = new InProgressFragmentPresenterImpl(this);
    }

    @Override
    public void initModel() {
        mPresenter.getModel();
    }

    @Override
    public void setModel(List<HandlingCardModel> model) {
        mModel = model;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDestroy();
        super.onDestroyView();
    }
}
