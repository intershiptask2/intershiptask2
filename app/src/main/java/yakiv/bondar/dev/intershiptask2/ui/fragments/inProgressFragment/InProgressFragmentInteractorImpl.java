package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

import java.util.ArrayList;
import java.util.List;

import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;
import yakiv.bondar.dev.intershiptask2.model.HandlingCardStatus;
import yakiv.bondar.dev.intershiptask2.data.ModelDataContainer;

/**
 * Created by User on 27.07.2016.
 */
public class InProgressFragmentInteractorImpl implements InProgressFragmentInteractor {

    //TODO:Check if singleton needed.
    @Override
    public void getModelFromArray(OnModelSetListener listener) {
        listener.onModelSet(getModelData());
    }

    private List<HandlingCardModel> getModelData() {
        List<HandlingCardModel> inProgressItems = new ArrayList<>(4);
        for(HandlingCardModel modelItem : ModelDataContainer.getsHandlingCardModels()){
            if (modelItem.getStatus() == HandlingCardStatus.IN_PROGRESS){
                inProgressItems.add(modelItem);
            }
        }
        return inProgressItems;
    }
}
