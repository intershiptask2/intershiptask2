package yakiv.bondar.dev.intershiptask2.model;

/**
 * Created by User on 26.07.2016.
 */
public enum HandlingCardStatus {
    IN_PROGRESS,
    DONE,
    WAITING
}
