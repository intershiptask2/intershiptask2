package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

import java.util.List;

import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;

/**
 * Created by User on 26.07.2016.
 */
public class InProgressFragmentPresenterImpl implements InProgressFragmentPresenter, InProgressFragmentInteractor.OnModelSetListener {

    InProgressFragmentView mView;
    InProgressFragmentInteractor mInteractor;

    public InProgressFragmentPresenterImpl(InProgressFragmentView mView) {
        this.mView = mView;
        mInteractor = new InProgressFragmentInteractorImpl();
    }

    @Override
    public void getModel() {
        mInteractor.getModelFromArray(this);
    }

    @Override
    public void onModelSet(List<HandlingCardModel> model) {
        mView.setModel(model);
    }

    @Override
    public void onDestroy() {
        mView = null;
    }
}
