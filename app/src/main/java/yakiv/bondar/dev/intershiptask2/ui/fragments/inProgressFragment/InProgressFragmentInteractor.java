package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

import java.util.List;

import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;

/**
 * Created by User on 27.07.2016.
 */
public interface InProgressFragmentInteractor {
    interface OnModelSetListener{
        void onModelSet(List<HandlingCardModel> model);
    }

    void getModelFromArray(OnModelSetListener listener);
}
