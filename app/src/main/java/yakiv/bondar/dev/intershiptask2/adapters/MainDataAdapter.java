package yakiv.bondar.dev.intershiptask2.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import yakiv.bondar.dev.intershiptask2.R;
import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;

/**
 * Created by User on 27.07.2016.
 */
public class MainDataAdapter extends RecyclerView.Adapter<MainDataAdapter.ViewHolder> {

    private List<HandlingCardModel> mModel;

    public MainDataAdapter(List<HandlingCardModel> mModel) {
        this.mModel = mModel;
    }

    @Override
    public MainDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_container, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainDataAdapter.ViewHolder holder, int position) {
        holder.tvTitle.setText(mModel.get(position).getmTitle());
        holder.tvDate.setText(mModel.get(position).getmDate());
        holder.tvAddress.setText(mModel.get(position).getmAddress());
        holder.tvLikesCount.setText(mModel.get(position).getmLikesCount()+"");
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.cv)
//        CardView cardView;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.tv_likes_count)
        TextView tvLikesCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
