package yakiv.bondar.dev.intershiptask2.model;

/**
 * Created by User on 26.07.2016.
 */
public class HandlingCardModel {
    private String mTitle;
    private String mAddress;
    private String mDate;
    private HandlingCardStatus status;
    private int mLikesCount;

    public HandlingCardModel(String mTitle, String mAddress, String mDate,
                             HandlingCardStatus status, int mLikesCount) {
        this.mTitle = mTitle;
        this.mAddress = mAddress;
        this.mDate = mDate;
        this.status = status;
        this.mLikesCount = mLikesCount;
    }

    public HandlingCardStatus getStatus() {
        return status;
    }

    public void setStatus(HandlingCardStatus status) {
        this.status = status;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public int getmLikesCount() {
        return mLikesCount;
    }

    public void setmLikesCount(int mLikesCount) {
        this.mLikesCount = mLikesCount;
    }
}
