package yakiv.bondar.dev.intershiptask2.ui.fragments.waitingFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yakiv.bondar.dev.intershiptask2.R;

/**
 * Created by User on 26.07.2016.
 */
public class WaitingFragmentViewImpl extends Fragment implements WaitingFragmentView {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.waiting_tab_fragment, container, false);
    }
}
