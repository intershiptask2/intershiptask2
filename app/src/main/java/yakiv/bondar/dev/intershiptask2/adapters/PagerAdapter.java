package yakiv.bondar.dev.intershiptask2.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import yakiv.bondar.dev.intershiptask2.ui.fragments.doneFragment.DoneFragmentViewImpl;
import yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment.InProgressFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                InProgressFragment inProgressFragmentView = new InProgressFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                inProgressFragmentView.setArguments(bundle);
                return inProgressFragmentView;
            //TODO: give position to Fragment to get data
            // from model by the filter (USE PRESENTER AND INTERACTOR TO GET DATA!)
            case 1:
                DoneFragmentViewImpl doneFragmentView = new DoneFragmentViewImpl();
                Bundle argsDone = new Bundle();
                argsDone.putInt("position", position);
                doneFragmentView.setArguments(argsDone);
                return doneFragmentView;
            case 2:
                DoneFragmentViewImpl waitingFragmentView = new DoneFragmentViewImpl();
                Bundle argsWaiting = new Bundle();
                argsWaiting.putInt("position", position);
                waitingFragmentView.setArguments(argsWaiting);
                return waitingFragmentView;
            default:
                Log.d("tag", "Wrong item position in PagerAdapter.");
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
