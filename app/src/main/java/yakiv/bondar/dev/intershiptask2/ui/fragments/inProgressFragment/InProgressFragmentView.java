package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

import java.util.List;

import yakiv.bondar.dev.intershiptask2.model.HandlingCardModel;

/**
 * Created by User on 26.07.2016.
 */
public interface InProgressFragmentView {
    void initModel();
    void setModel(List<HandlingCardModel> model);
}
