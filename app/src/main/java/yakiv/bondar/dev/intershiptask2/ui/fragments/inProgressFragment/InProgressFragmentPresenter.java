package yakiv.bondar.dev.intershiptask2.ui.fragments.inProgressFragment;

/**
 * Created by User on 26.07.2016.
 */
public interface InProgressFragmentPresenter {

    void getModel();

    void onDestroy();
}
