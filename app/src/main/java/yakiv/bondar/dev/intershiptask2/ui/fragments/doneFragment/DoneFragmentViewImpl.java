package yakiv.bondar.dev.intershiptask2.ui.fragments.doneFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import yakiv.bondar.dev.intershiptask2.R;

/**
 * Created by User on 26.07.2016.
 */
public class DoneFragmentViewImpl extends Fragment implements DoneFragmentView {

    //TODO: init data in onCreate
    //TODO: i

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.done_tab_fragment, container, false);
    }
}
